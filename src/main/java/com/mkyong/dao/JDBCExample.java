package com.mkyong.dao;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.mkyong.model.User;

public class JDBCExample {

	public static void main(String[] args) {

		System.out.println("MySQL JDBC Connection Testing ~");

		List<User> result = new ArrayList<>();

		String SQL_SELECT = "Select * from user";

		try (Connection conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/dacnpm", "root", "123456");
			PreparedStatement preparedStatement = conn.prepareStatement(SQL_SELECT)) {

			ResultSet resultSet = preparedStatement.executeQuery();

			System.out.println("Test data");
			while (resultSet.next()) {
				int userid = resultSet.getInt("userid");
				String username = resultSet.getString("username");
				String userpassword = resultSet.getString("userpassword");

				User user = new User();
				user.setUserid(userid);
				user.setUsername(username);
				user.setUserpassword(userpassword);
				result.add(user);

			}
			for (User user : result) {
				System.out.println(user.getUsername());
			}

		} catch (SQLException e) {
			System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
